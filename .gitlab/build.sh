#!/bin/bash
#
# Build Script to transform DocBook file into HTML and PDF
#
# Written March 2018 by Thomas Schraitle

PROG=$(readlink ${0##*/})

# Directories:
PROGDIR=$(realpath -P ${PROG%/*})
ROOTDIR=$(realpath $PROGDIR/.. )
TARGETDIR=$(realpath ${ROOTDIR}/build)/

# RNG-Schema
RNG="/usr/share/xml/docbook/schema/rng/5.1/docbook.rng"

# Stylesheets
XSLTFO="${ROOTDIR}/tutorial/docbook-tutorial-fo.xsl"
XSLTHTML="${ROOTDIR}/tutorial/docbook-tutorial-html.xsl"
XSLTROOT="/usr/share/xml/docbook/stylesheet/nwalsh5/current"

# Files
BASEXML="docbook-tutorial.xml"
XML="${ROOTDIR}/tutorial/$BASEXML"
DESTXML="$TARGETDIR${BASEXML%.xml}-xi.xml"
DESTFO="$TARGETDIR${BASEXML%.xml}.fo"
DESTHTML="$TARGETDIR${BASEXML%.xml}.html"
DESTPDF="$TARGETDIR${BASEXML%.xml}.pdf"


# Flags
SKIPVALIDATION="yes"

# Build scripts
VALIDATOR="jing"
TRANSFORMATOR="saxon6"
FORMATTER="fop"

export CLASSPATH="$PROGDIR/lib/db-saxon65.jar"
# export VERBOSE=1

function loginfo() {
    local extraoptions=
    if [[ $1 == '-n' ]]; then
        extraoptions='-n'
        shift
    fi
    echo -e $extraoptions "\e[1;39mINFO: $1\e[0m"
}

function logwarn() {
    echo -e "\e[1;95mWARN: $1\e[0m"
}

print_vars() {
    cat <<EOF
PROG=$PROG
PROGDIR=$PROGDIR
VALIDATOR=$VALIDATOR
TRANSFORMATOR=$TRANSFORMATOR
FORMATTER=$FORMATTER

XML=$XML

DESTXML=$DESTXML
DESTFO=$DESTFO
DESTHTML=$DESTHTML
DESTPDF=$DESTPDF

XSLTROOT=$XSLTROOT
XSLTFO=$XSLTFO
XSLTHTML=$XSLTHTML
EOF
}

print_help() {
    cat <<EOF_helptext
Validate and transform DocBook document

Usage:
   ${PROG} [-h|--help] [OPTIONS]

Options:
   -h, --help  Shows this help message
   -V VALIDATOR, --validator VALIDATOR
               Choose a XML validator, use either "jing" or "xmllint"
   -T TRANS, --transformator TRANS
               Choose an XSLT processor, use either "xsltproc" or "saxon6"
   -F FORMATTER, --formatter FORMATTER
               Choose a FO formatter, use either "fop" or "xep"
EOF_helptext
}


check_result() {
  local result=$1
  local badmsg=${2:-ERROR: Transformation problem}
  local goodmsg=${3:-=> successful}

  if [ 0 -ne $result ]; then
     echo "$badmsg"
     exit 10
  else
     loginfo "$goodmsg"
     echo
  fi
}

validator() {
    local rng=$1
    local xml=$2

    loginfo "## Validation with $VALIDATOR..."
    if [ "jing" == "$VALIDATOR" ]; then
        jing $rng $xml
    elif [ "xmllint" == "$VALIDATOR" ]; then
        # echo xmllint --stream --noout --xinclude --relaxng $RNG $XML
        logwarn "   Does not work at the moment. Using 'jing'"
        jing $rng $xml
    fi

    check_result $? "ERROR: Validation problem"
}

xslttrans() {
  local format=$1
  local xslt=$2
  local xml=$3
  local out=$4

  loginfo "## Transformation $format..."
  if [ "xsltproc" == "$TRANSFORMATOR" ]; then
     xsltproc --xinclude --output $out $xslt $xml
  elif [ "saxon6" == "$TRANSFORMATOR" ]; then
     saxon6 -o $out "$xml" $xslt
  fi
  check_result $?
}

foformatter() {
    local fo=$1
    local pdf=$2

    loginfo "## Formatting FO..."
    if [ "fop" == "$FORMATTER" ]; then
        fop $fo $pdf
    elif [ "xep" == "$FORMATTER" ]; then
        xep $fo $pdf
    fi

    check_result $?
}

initbuilddir() {
    local xml=$1
    local destxml=$2

    loginfo "## Init build dir..."
    [[ -e ${TARGETDIR} ]] || mkdir -p ${TARGETDIR}
    pushd ${TARGETDIR} 2>/dev/null
        ln -sf $XSLTROOT/images .
         xmllint --xinclude --output "$destxml" "$xml"
    popd 2>/dev/null
}


ARGS=$(getopt -o h,V:,T:,F: -l help,validator:transformator:,formatter: -n "$PROG" -- "$@")
eval set -- "$ARGS"
while true ; do
    case "$1" in
        -h|--help)
            print_help
            exit
            shift
            ;;
        -V|--validator)
            case "$2" in
                'xmllint' | 'jing')
                 VALIDATOR="$2"
                 shift 2
                 ;;
               *)
                 logerror "Unrecognized validator '$2'" 2>/dev/stderr
                 exit 10
                 ;;
            esac
            ;;
        -T|--transformator)
            case "$2" in
                'xsltproc' | 'saxon6')
                 TRANSFORMATOR="$2"
                 shift 2
                 ;;
               *)
                 logerror "Unrecognized XSLT processor '$2'" 2>/dev/stderr
                 exit 10
                 ;;
            esac
            ;;
        -F|--formatter)
            case "$2" in
                'fop' | 'xep')
                 FORMATTER="$2"
                 shift 2
                 ;;
               *)
                 logerror "Unrecognized formatter '$2'" 2>/dev/stderr
                 exit 10
                 ;;
            esac
            ;;
        --)
            shift
            break
            ;;
    esac
done


print_vars
initbuilddir $XML $DESTXML

if [ "no" = "$SKIPVALIDATION" ]; then
    validator $RNG $DESTXML
fi

xslttrans "PDF" $XSLTFO $DESTXML $DESTFO

foformatter $DESTFO $DESTPDF

xslttrans "HTML" $XSLTHTML $DESTXML $DESTHTML

loginfo "Find the result here:"
loginfo "> $DESTHTML"
loginfo "> $DESTPDF"
# EOF
