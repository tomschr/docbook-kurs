#!/bin/bash

source /etc/os-release

BASEURL="https://download.opensuse.org/repositories"

if [[ $NAME = *"Tumbleweed"* ]]; then
  OSC_VERSION="openSUSE_Tumbleweed/"
elif [[ $NAME = *"Leap"* ]]; then
  OSC_VERSION="openSUSE_Leap_\$releasever/"
else
  echo "ERROR: Probably an old openSUSE version?"
  echo "The script $0 supports only Leap and Tumbleweed"
  exit 10
fi


# Add some repositories
zypper -n ar -G $BASEURL/Publishing/$OSC_VERSION  Publishing
zypper -n ar -G $BASEURL/Documentation:/Tools/$OSC_VERSION Documentation:Tools
zypper -n ar -G $BASEURL/M17N:/fonts/$OSC_VERSION M17N:fonts


# Install packages
zypper -n install --no-recommends docbook_5 docbook5-xsl-stylesheets \
      jing xmlgraphics-fop xerces-j2 saxon6-scripts relaxngDatatype \
      libxml2-tools libxslt-tools \
      google-montserrat-fonts ibm-plex-mono-fonts ibm-plex-serif-fonts gdouros-symbola-fonts
