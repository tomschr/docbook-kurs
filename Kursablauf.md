# DocBook-Kurs

## Einleitung

* Vorstellung Dozent und Teilnehmer

* Fragerunde an die Teilnehmer:
  * Warum dieser Kurs?
  * Erwartungen an den Kurs
  * Was wollen sie mitnehmen?
  * Fragen und Wünsche die im Kurs beantwortet werden soll(t)en

* Organisatorisches (Duzen?, Pausen, Mittagessen, Kursende, Fragen nach den Kurs)

* Voraussetzungen klären:
  * XML: Element, Attribut, Start/End-Tag, Entity, PI
  * Namensräume
  * Schemata (DTD, RELAX NG, W3C Schema, ...)


## Teil I: Markup

* DocBook
  * Markupsprache für Dokumentation von Hard- und Software
  * Verfügbar in Version 4 (DTD) und 5 (RELAX NG)

* Verschiedene "Typen" von Elementen:
  Struktur-, Block-, Inlineelemente

* Strukturelemente:
  * Artikel (`<article>`)
  * Bücher (`<book>`)
  * Teile (`<part>`)
  * Glossare (`<glossary>`)

* Blockelemente
  * Abbildungen (`<figure>`)
  * Tabellen (`<table>`)
  * Beispiele (`<example>`)
  * Handlungsanweisungen (`<procedure>`)
  * Warnhinweise (`<tip>`, `<warning>` usw.)

* Modulare Dokumente
  * externe Entities vs. XIncludes


## Teil II. Transformation

* Umwandlung nach HTML
* Umwandlung nach PDF (über FO)
* Kleine Anpassungen an den DocBook-Stylesheets
