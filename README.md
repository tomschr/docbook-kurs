# Übersicht

[![Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

---

Dieses Repository ist für den DocBook-Kurs bei data2type gedacht.


## Dateien


* [`DocBook-Kurs.xpr`](DocBook-Kurs.xpr)

    Projektdatei für den oXygen XML-Editor. Diese Datei enthält bereits alle
    Einstellungen zum Validieren und Transformieren.
* [`tutorial/docbook-tutorial.xml`](tutorial/docbook-tutorial.xml)

    Hauptdatei für dieses Tutorial
* [`tutorial/loesungen/`](tutorial/loesungen/)

    Verzeichnis aller Lösungen
  

## Validieren und Transformieren

Um mit Hilfe des oXygen XML-Editors die Datei verarbeiten zu können, gehen
Sie wie folgt vor:

1. Laden Sie die Projektdatei `DocBook-Kurs.xpr` in oXygen. Verwenden Sie
   den Menüpunkt `Projekt` > `Projekt öffnen...`.
   Sie sollten nun die Tutorial-Datei in oXygen sehen.
2. Validieren Sie die Datei über den Menüpunkt `Dokument` > `Validieren` >
   `Validieren` oder drücken Sie Strg+Umschalt+V.
3. Transformieren Sie die Datei nach HTML und PDF über den Menüpunkt
   `Dokument` > `Transformation` > `Transformations-Szenario anwenden...` oder
   drücken Sie Strg+Umschalt+T.
4. Überprüfen Sie das Ergebnis im Verzeichnis `build`. Dort sollten eine HTML-
   und PDF-Datei vorliegen.

Um HTML und PDF ohne oXygen zu bauen, verwenden Sie das `build.sh` Skript.
Dieses Skript ist für openSUSE geschrieben; mit minimalen Anpassungen kann
es auch für andere Distributionen verwendet werden. Feedback willkommen!


## Downloads

Das HTML und PDF kann im jeweiligen [Repository > Tag](https://gitlab.com/tomschr/docbook-kurs/tags)-Bereich heruntergeladen werden.
