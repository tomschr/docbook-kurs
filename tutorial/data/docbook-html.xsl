<!DOCTYPE xsl:stylesheet [
  <!ENTITY dbxsl "http://docbook.sourceforge.net/release/xsl-ns/current">
]>
<xsl:stylesheet version="1.0" 
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
   xmlns:d="http://docbook.org/ns/docbook"
   xmlns="http://www.w3.org/1999/xlink"
   exclude-result-prefixes="d xlink"> 
 
   <xsl:import href="&dbxsl;/xhtml/docbook.xsl"/>

   <!-- Überschreiben von Parametern -->
   <xsl:param name="section.autolabel" select="1"/>

   <!-- Überschreiben eines Templates -->
   <xsl:template name="user.head.content">
      <xsl:param name="node" select="."/>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   </xsl:template>

   <xsl:template match="d:command">
      <i><xsl:apply-templates/></i>
   </xsl:template>
</xsl:stylesheet>