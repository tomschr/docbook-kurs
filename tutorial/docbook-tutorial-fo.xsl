<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
   xmlns:d="http://docbook.org/ns/docbook"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:import href="http://docbook.sourceforge.net/release/xsl-ns/current/fo/docbook.xsl"/>

   <xsl:param name="fop1.extensions" select="1"/>
   <xsl:param name="paper.type" select="'A4'"/>
   <xsl:param name="section.autolabel" select="1"/>
   <xsl:param name="use.extensions">
      <xsl:variable name="vendor" select="system-property('vendor')"/>
      <xsl:choose>
         <xsl:when test="contains($vendor, 'libxslt')">0</xsl:when>
         <xsl:when test="contains($vendor, 'saxon')">1</xsl:when>
         <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
   </xsl:param>
   <xsl:param name="variablelist.as.blocks" select="1"/>

   <xsl:param name="sans.font.family">Montserrat</xsl:param>
   <!-- ,Montserrat-Regular,SourceSansPro-Regular -->
   <xsl:param name="title.font.family">Montserrat</xsl:param>
   <xsl:param name="body.font.family">IBMPlexSerif-Regular</xsl:param>
   <xsl:param name="monospace.font.family">IBMPlexMono-Text</xsl:param>
   <xsl:param name="symbol.font.family">Symbola,<xsl:value-of select="$sans.font.family"/></xsl:param>
   <xsl:param name="dingbat.font.family" select="$symbol.font.family"/>
   <xsl:param name="callout.unicode.font" select="$symbol.font.family"/>
   <xsl:param name="callout.unicode" select="1"/>
   <xsl:param name="callout.graphics" select="0"/>
<!--   <xsl:param name="ulink.footnotes" select="1"/>-->
   <xsl:param name="callouts.extension" select="1"/>
<!--   <xsl:param name="hyphenate.verbatim" select="1"/>-->
  
  <!-- ####################################################### -->
  <xsl:attribute-set name="formal.title.properties">
    <xsl:attribute name="font-family">Montserrat-Medium</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="monospace.verbatim.properties"
    use-attribute-sets="verbatim.properties monospace.properties">
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="wrap-option">wrap</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="section.title.properties">
    <xsl:attribute name="font-family">Montserrat-Medium</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="hyphenate">false</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
<!--    <xsl:attribute name="color">#00007f</xsl:attribute>-->
  </xsl:attribute-set>
  <xsl:attribute-set name="section.title.level1.properties">
    <xsl:attribute name="border-top">1pt dotted gray</xsl:attribute>
    <xsl:attribute name="padding-top">4pt</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="article.titlepage.recto.style">
    <xsl:attribute name="font-family">Montserrat-Bold</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="text-align">center</xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:attribute-set name="admonition.properties">
    <xsl:attribute name="border-left">1pt solid gray</xsl:attribute>
    <xsl:attribute name="padding-left">4pt</xsl:attribute>
    <xsl:attribute name="margin-left">1pt</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="admonition.title.properties">
    <xsl:attribute name="font-family">Montserrat-Medium</xsl:attribute>
    <xsl:attribute name="hyphenate">false</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="link.url.properties">
    <xsl:attribute name="font-family">IBMPlexMono-Regular,<xsl:value-of 
      select="$monospace.font.family"/></xsl:attribute>
    <xsl:attribute name="hyphenate">false</xsl:attribute>
    <xsl:attribute name="wrap-option">wrap</xsl:attribute>
  </xsl:attribute-set>

  <!-- ####################################################### -->
  <xsl:template name="table.of.contents.titlepage.recto">
    <fo:block xsl:use-attribute-sets="table.of.contents.titlepage.recto.style section.title.properties"
      space-before.minimum="1em" space-before.optimum="1.5em"
      space-before.maximum="2em"  space-after="0.5em" 
      start-indent="0pt" font-size="17.28pt">
      <xsl:call-template name="gentext">
        <xsl:with-param name="key" select="'TableofContents'"/>
      </xsl:call-template>
    </fo:block>
  </xsl:template>

  <xsl:template name="hyperlink.url.display">
    <!-- * This template is called for all external hyperlinks (ulinks and -->
    <!-- * for all simple xlinks); it determines whether the URL for the -->
    <!-- * hyperlink is displayed, and how to display it (either inline or -->
    <!-- * as a numbered footnote). -->
    <xsl:param name="url"/>
    <xsl:param name="ulink.url">
      <!-- * ulink.url is just the value of the URL wrapped in 'url(...)' -->
      <xsl:call-template name="fo-external-image">
        <xsl:with-param name="filename" select="$url"/>
      </xsl:call-template>
    </xsl:param>
    
    <xsl:if test="count(child::node()) != 0 and string(.) != $url and $ulink.show != 0">
      <!-- * Display the URL for this hyperlink only if it is non-empty, -->
      <!-- * and the value of its content is not a URL that is the same as -->
      <!-- * URL it links to, and if ulink.show is non-zero. -->
      <xsl:choose>
        <xsl:when test="$ulink.footnotes != 0 and not(ancestor::d:footnote) and not(ancestor::*[@floatstyle='before'])">
          <!-- * ulink.show and ulink.footnote are both non-zero; that -->
          <!-- * means we display the URL as a footnote (instead of inline) -->
          <fo:footnote>
            <xsl:call-template name="ulink.footnote.number"/>
            <fo:footnote-body xsl:use-attribute-sets="footnote.properties">
              <fo:block>
                <xsl:call-template name="ulink.footnote.number"/>
                <xsl:text> </xsl:text>
                <fo:basic-link external-destination="{$ulink.url}">
                  <xsl:value-of select="$url"/>
                </fo:basic-link>
              </fo:block>
            </fo:footnote-body>
          </fo:footnote>
        </xsl:when>
        <xsl:otherwise>
          <!-- * ulink.show is non-zero, but ulink.footnote is not; that -->
          <!-- * means we display the URL inline -->
          <fo:inline hyphenate="false" font-style="normal">
            <!-- * put square brackets around the URL -->
            <xsl:text> [</xsl:text>
            <fo:inline xsl:use-attribute-sets="link.url.properties">
            <fo:basic-link external-destination="{$ulink.url}">
              <xsl:call-template name="hyphenate-url">
                <xsl:with-param name="url" select="$url"/>
              </xsl:call-template>
            </fo:basic-link>
            </fo:inline>
            <xsl:text>]</xsl:text>
          </fo:inline>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <!-- ####################################################### -->
  <xsl:template name="inline.boldmonoseq">
    <xsl:param name="content">
      <xsl:apply-templates/>
    </xsl:param>

    <xsl:param name="contentwithlink">
      <xsl:call-template name="simple.xlink">
        <xsl:with-param name="content" select="$content"/>
      </xsl:call-template>
    </xsl:param>

    <fo:inline font-family="IBMPlexSans-Bold">
      <xsl:call-template name="anchor"/>
      <xsl:if test="@dir">
        <xsl:attribute name="direction">
          <xsl:choose>
            <xsl:when test="@dir = 'ltr' or @dir = 'lro'">ltr</xsl:when>
            <xsl:otherwise>rtl</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </xsl:if>
      <xsl:copy-of select="$contentwithlink"/>
    </fo:inline>
  </xsl:template>

  <xsl:template name="inline.italicmonoseq">
    <xsl:param name="content">
      <xsl:apply-templates/>
    </xsl:param>

    <xsl:param name="contentwithlink">
      <xsl:call-template name="simple.xlink">
        <xsl:with-param name="content" select="$content"/>
      </xsl:call-template>
    </xsl:param>

    <fo:inline font-family="IBMPlexMono-TextItalic">
      <xsl:call-template name="anchor"/>
      <xsl:if test="@dir">
        <xsl:attribute name="direction">
          <xsl:choose>
            <xsl:when test="@dir = 'ltr' or @dir = 'lro'">ltr</xsl:when>
            <xsl:otherwise>rtl</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </xsl:if>
      <xsl:copy-of select="$contentwithlink"/>
    </fo:inline>
  </xsl:template>

  <xsl:template match="d:para/d:emphasis | d:para/d:foreignphrase | d:para/d:citetitle">
    <fo:inline font-family="IBMPlexSerif-Italic">
      <xsl:call-template name="inline.charseq"/>
    </fo:inline>
  </xsl:template>

  <xsl:template match="d:guibutton">
    <fo:inline border="1pt solid gray" padding=".9pt">
      <xsl:apply-imports/>
    </fo:inline>
  </xsl:template>
  
  <xsl:template match="d:guilabel">
    <fo:inline color="green">
      <xsl:apply-imports/>
    </fo:inline>
  </xsl:template>
  
  <xsl:template match="d:guimenu|d:guisubmenu|d:guimenuitem">
    <fo:inline color="#00002f">
      <xsl:call-template name="inline.charseq"/>
    </fo:inline>
  </xsl:template>
</xsl:stylesheet>