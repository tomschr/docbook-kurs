<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns="http://www.w3.org/1999/xhtml">

<xsl:import href="http://docbook.sourceforge.net/release/xsl-ns/current/xhtml/docbook.xsl"/>

   <xsl:param name="callouts.extension" select="1"/>
   <xsl:param name="section.autolabel" select="1"/>
   <xsl:param name="use.extensions" select="1"/>
   <xsl:template name="user.head.content">
      <xsl:param name="node" select="."/>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   </xsl:template>

</xsl:stylesheet>